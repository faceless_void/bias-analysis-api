import json
import glob
import traceback
from gensim.models import KeyedVectors
from calculate_distance import CalculateDistance
from calculate_bias import calculate_bias_for_all_embedding
from utils import populate_b2e_dict_for_words
from utils import get_sorted_dict
import json



def get_bias_from_calculate_class(word_embeddings, reference_groups, target_group, embedding_directory):
    messages = {}
    print(type(target_group), target_group)
    # print(json.loads(target_group))
    target_group_words = [word.strip() for word in target_group['word_list'].split(',')]
    target_group_words_english = [word.strip() for word in target_group['word_list_english'].split(',')]
    print(target_group_words, target_group_words_english, 'before b2e_dict')
    b2e_dict = populate_b2e_dict_for_words(target_group_words, target_group_words_english)

    target_group_words_english = [target_group_words_english]
    target_group_words = [target_group_words]
    # print('target_group_words_here', target_group_words)
    # print('target_group_english here ', target_group_words_english)
    
    target_group_words_label = [target_group['label']]

    if len(target_group_words) != len(target_group_words_english):
        messages['word_list_error']= "Target group and corresponding english word are not equal, target group word {} and english {}".format(len(target_group_words), len(target_group_words_english))
        return {}, messages


    reference_group_words = []
    reference_group_labels = []
    
    for key, reference_group in reference_groups.items():
        reference_group_words.append([word.strip() for word in reference_group['word_list'].split(',')])
        reference_group_labels.append(reference_group['label'])
    
    # print('target group -->')
    # for group, labels in zip(target_group_words, target_group_words_label):
    #     print(labels, group)

    # print('reference group -->')
    # for group, labels in zip(reference_group_words, reference_group_labels):
    #     print(labels, group)

    # b2e_dict = populate_b2e_dict_for_words(target_group_words, target_group_words_english)

    calculate_association_distance = CalculateDistance()
    calculate_association_distance.set_target_word_groups(target_word_groups=target_group_words, target_group_labels=target_group_words_label)
    calculate_association_distance.set_reference_word_groups(reference_word_groups=reference_group_words, reference_group_labels=reference_group_labels)

    association_dict, messages = get_association_from_embeddings(word_embeddings, calculate_association_distance, embedding_directory, messages)

    # print(target_group_words_label[0], 'before calling bias')
    # print(reference_group_labels, 'reference label before calling')
    bias_dict_for_all_embeddings = get_bias_from_association(association_dict=association_dict, 
                                                                     target_label = target_group_words_label[0], 
                                                                     reference_labels = reference_group_labels)
    # print('bias dict before sorting {}'.format(bias_dict_for_all_embeddings))
    bias_dict_for_all_embeddings_sorted = get_sorted_bias_dict(bias_dict_for_all_embeddings, b2e_dict)
    # print('bias dict after sorting {}'.format(bias_dict_for_all_embeddings))
    for key in bias_dict_for_all_embeddings.keys():
        if key not in bias_dict_for_all_embeddings_sorted.keys():
            bias_dict_for_all_embeddings_sorted[key] = {}
    messages['legend'] = reference_group_labels[1] + ' bias'
    return bias_dict_for_all_embeddings_sorted, messages

def get_association_from_embeddings(word_embeddings, calculate_association_distance, embedding_directory, messages):
    association_dict = {}
    

    for filename in word_embeddings:
        print('working on {} embedding '.format(filename))
        newspaper_name = filename
        base_file_name = filename
        messages[base_file_name] = {}
        filename = embedding_directory + '/' + filename
        target_wise_association_for_this_paper = {}
        # association_dict[newspaper_name] = {}
        try:
            if filename.split('.')[-1] =='bin':
                model = KeyedVectors.load_word2vec_format(filename, binary=True)
            else:
                model = KeyedVectors.load_word2vec_format(filename, binary=False)

            target_wise_association_for_this_paper = calculate_association_distance.get_association_for_provided_embedding(model=model)
            association_dict[newspaper_name] = target_wise_association_for_this_paper
            
            print('from association calculation {}'.format(association_dict))
            if len(calculate_association_distance.failed_words) != 0:
                messages[base_file_name]['missing_words_error'] = 'words that are not in embedding, {}'.format(calculate_association_distance.failed_words)
        except:
            traceback.print_exc(limit=3)
            association_dict[newspaper_name] = {}
            messages[base_file_name]['embedding_error']='Embedding {} loading failed'.format(filename)
        
        
    return association_dict, messages


def get_sorted_bias_dict(bias_dict_for_all_embedding, b2e_dict):
    updated_bias_dict = {}
    for key in bias_dict_for_all_embedding.keys():
        sorted_dict = get_sorted_dict(bias_dict_for_all_embedding[key])
        updated_dict_this_embedding = {}
        x_labels = [b2e_dict[word] for word in sorted_dict.keys()]
        y_labels = list(sorted_dict.values())
        for k, v in zip(x_labels, y_labels):
            updated_dict_this_embedding[k] = v
        updated_bias_dict[key] = updated_dict_this_embedding
        return updated_bias_dict

    


def get_bias_from_association(association_dict, target_label, reference_labels):
    
    bias_dict_for_all_embeddings = calculate_bias_for_all_embedding(association_dict=association_dict, 
                                                                     target_label = target_label, 
                                                                     reference_labels = reference_labels)
    return bias_dict_for_all_embeddings

    
