import flask
from flask import render_template
from flask import Flask, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from api_utils import allowed_file
import os
from dotenv import load_dotenv
from bias_calculation_interface import get_bias_from_calculate_class
import json
from urllib.parse import parse_qs
from flask_cors import CORS, cross_origin


load_dotenv()

app = flask.Flask(__name__, static_url_path='')
app.config["DEBUG"] = False
app.config['MAX_CONTENT_LENGTH'] = 3 * 1024 * 1024 * 1024




app.config['UPLOAD_FOLDER'] = os.getenv('UPLOAD_FOLDER')

CORS(app)


@app.route('/', methods=['GET'])
def home():
    import os
    embedding_list = os.listdir(os.getenv('UPLOAD_FOLDER'))
    embedding_list = [f for f in embedding_list  if not f.startswith('.')]
    reference_group = {}
    target_group = {}

    data = {}
    data['embedding_list'] = embedding_list
    # data['reference_group'] = reference_group
    # data['target_group'] = target_group

    return flask.jsonify(data)

@app.route('/get_bias_result', methods=['GET','POST'])
@cross_origin()
def get_bias_result():
    print('request data {}'.format(request.data))
    # print('request data {}'.format(json.loads(request.data, encoding='utf-8')))
    query_string = request.query_string
    print('query string {}'.format(query_string))

    if request.method == 'POST':
        print('post request recieved')
        # print(request.form.get('embeddings'), 'embeddingssssssssssssssss')
        embedding_names = request.form['embeddings']
        reference_groups = request.form.get("reference_group")
        target_group = request.form.get("target_group")
    # print(request.get_json())
    
    
    else:
        embedding_names = json.loads(request.args.get('embedding_list').replace('\'', '"'))['embeddings']
        reference_groups = json.loads(request.args.get('reference_group').replace('\'', '"'))
        # print(reference_groups)
        target_group = json.loads(request.args.get('target_group').replace('\'', '"'))
    # print('\n' * 2)
    # print('embedding ', (embedding_names , type(embedding_names)))
    # print('target group ',target_group, type(target_group))
    # print('reference_group ', reference_groups, type(reference_groups))

    # print('\n' * 2)


    
    bias_dict_for_all_embeddings, message = get_bias_from_calculate_class(embedding_names, reference_groups, target_group, os.getenv('UPLOAD_FOLDER'))
    # print(embedding_names, reference_groups, target_group)

    results = {
        'bias_dict': bias_dict_for_all_embeddings,
        'messages': message
    }
    # print(results)
    return flask.jsonify(results)







@app.route('/file_upload', methods=['GET', 'POST'])
@cross_origin()
def upload_file():
    if request.method == 'POST':
        print('post method called')
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            embedding_list = os.listdir(os.getenv('UPLOAD_FOLDER'))
            if file.filename in embedding_list:
                return "embedding already exists"
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return "success"
    return '''
    <!doctype html>
    <title>Upload new File</title>
    <h1>Upload new File</h1>
    <form method=post action="/file_upload" enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    '''

app.config['JSON_AS_ASCII'] = False
app.config['JSON_SORT_KEYS'] = False
app.run(host='0.0.0.0', port=os.getenv('PORT'))
