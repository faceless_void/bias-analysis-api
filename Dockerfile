FROM python:3.5 
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt 
EXPOSE 5067 
ENTRYPOINT [ "python" ] 
CMD [ "api.py" ] 